Eksami alguses peaksite käivitama käsu

  java -jar monitor.jar <teie bitbuket-i kasutajatunnus>

  (olles kataloogis, kuhu te projekti kloonisite).

See programm esitab teie töö automaatselt. Soovitatav on kontrollida
(nupp "Show Uploaded Data"), kas see ka nii on.

Küsimused laeb monitor alla (faili questions.txt) siis, kui esimene ekraanitõmmis
on üles saadetud.

Aega on 100 minutit. Arvesse läheb seis, mis on esitatud enne kella 15:40.

Teooriaküsimustele saate vastata 20 minuti jooksul alates eksami algusest (14:00).
Muudatused, mis on tehtud hiljem kui 14:20 ei lähe arvesse.
